from django.conf.urls import patterns, include, url
from rest_app.api import UserResource

usr_resource = UserResource()

urlpatterns = patterns('', 
    url(r'^usr/', include(usr_resource.urls)),
)

